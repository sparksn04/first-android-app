package wix.sparksn04.com.androidintroduciton;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
private Button button1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1 = (Button)findViewById(R.id.button1);

        button1.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        Toast toast;

        switch (view.getId())
        {
            case R.id.button1:
                toast = Toast.makeText(this,"Button pushed",Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER,25, 400);
                toast.show();

                break;


        }

    }
}
